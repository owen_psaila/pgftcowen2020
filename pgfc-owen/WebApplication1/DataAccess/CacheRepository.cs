﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Web;
using StackExchange.Redis;
using WebApplication1.Models;

namespace WebApplication1.DataAccess
{
    public class CacheRepository
    {

        private IDatabase db;
        public CacheRepository()
        {
            // var connection = ConnectionMultiplexer.Connect("localhost"); //localhost if cache server is installed locally after downloaded from https://github.com/rgl/redis/downloads 
            // connection to your REDISLABS.com db as in the next line NOTE: DO NOT USE MY CONNECTION BECAUSE I HAVE A LIMIT OF 30MB...CREATE YOUR OWN ACCOUNT
            var connection = ConnectionMultiplexer.Connect("redis-11561.c228.us-central1-1.gce.cloud.redislabs.com:11561,password=SR31gPK47NehtDRf24jjPuigt0gEeX7W");
            db = connection.GetDatabase();
        }

        /// <summary>
        /// store a list of products in cache
        /// </summary>
        /// <param name="products"></param>
        public void UpdateCache(List<File> products)
        {
            if (db.KeyExists("products") == true)
                db.KeyDelete("products");

            string jsonString;
            jsonString = JsonSerializer.Serialize(products); 

            db.StringSet("products", jsonString);
        }
        /// <summary>
        /// Gets a list of products from cache
        /// </summary>
        /// <returns></returns>
        public List<File> GetProductsFromCache(string email)
        {
            if (db.KeyExists("products") == true)
            {
                var products = JsonSerializer.Deserialize<List<File>>(db.StringGet("products")).Where(x => x.OwnerFk == email).ToList();
                return products;
            }
            else
            {
                return new List<File>();
            }
        }
    }
}