﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.DataAccess
{
    public class FilesRepository: ConnectionClass
    {
        public FilesRepository() : base() { }


        public void AddFile(File p)
        {
            string sql = "INSERT into files(name,ownerfk,sharewith,link) values(@n,@o,@s,@l)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@n", p.Name);
            cmd.Parameters.AddWithValue("@o", p.OwnerFk);
            cmd.Parameters.AddWithValue("@s", p.Sharewith);
            cmd.Parameters.AddWithValue("@l", p.Link);

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }

            cmd.ExecuteNonQuery();

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }

        }

        public List<File> GetFiles()
        {
            string sql = "Select id, name, ownerfk, sharewith, link from files";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);


            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }

            List<File> results = new List<File>();

            using (var reader = cmd.ExecuteReader())
            {
                while(reader.Read())
                {
                    File p = new File();
                    p.Id = reader.GetInt32(0);
                    p.Name = reader.GetString(1);
                    p.OwnerFk = reader.GetString(2);
                    p.Sharewith = reader.GetString(3);
                    p.Link = reader.GetString(4);
                    results.Add(p);
                }
            }

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }

            return results;
        }



        public List<File> GetFiles(string email)
        {
            string sql = "Select id, name, ownerfk, sharewith, link from files where ownerfk=@email or sharewith=@email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }
            List<File> results = new List<File>();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    File p = new File();
                    p.Id = reader.GetInt32(0);
                    p.Name = reader.GetString(1);
                    p.OwnerFk = reader.GetString(2);
                    p.Sharewith = reader.GetString(3);
                    p.Link = reader.GetString(4);
                    results.Add(p);
                }
            }

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }

            return results;
        }

        public File GetFile(int id, string owner, string read)
        {
            string sql = "Select id, name, ownerfk, sharewith, link from files where id=@id and (ownerfk=@o or sharewith=@r)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@o", owner);
            cmd.Parameters.AddWithValue("@r", read);

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }
            File p = new File();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    p.Id = reader.GetInt32(0);
                    p.Name = reader.GetString(1);
                    p.OwnerFk = reader.GetString(2);
                    p.Sharewith = reader.GetString(3);
                    p.Link = reader.GetString(4);
                }
            }

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }

            return p;
        }





        public void DeleteFile(int id)
        {
            string sql = "Delete from files where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if(MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }
            cmd.ExecuteNonQuery();

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }
        }






    }
}