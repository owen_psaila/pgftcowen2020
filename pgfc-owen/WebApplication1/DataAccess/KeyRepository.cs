﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Cloud.Kms.V1;
using System.IO;
using Google.Protobuf;
using System.Text;

namespace WebApplication1.DataAccess
{
    public class KeyRepository
    {
        public static string Encrypt(string plaintext) {
            KeyManagementServiceClient client = KeyManagementServiceClient.Create();

            CryptoKeyName kn = CryptoKeyName.FromUnparsed(
                new Google.Api.Gax.UnparsedResourceName("projects/owenprogforthecloud2020/locations/global/keyRings/owenpfc1/cryptoKeys/pfcKey")
                );
            string cipher = client.Encrypt(kn, ByteString.CopyFromUtf8(plaintext)).Ciphertext.ToBase64();

            return cipher;
        }

       /* public static string Decrypt(string plaintext)
        {
            KeyManagementServiceClient client = KeyManagementServiceClient.Create();

            CryptoKeyName kn = CryptoKeyName.FromUnparsed(
                new Google.Api.Gax.UnparsedResourceName("projects/owenprogforthecloud2020/locations/global/keyRings/owenpfc1/cryptoKeys/pfcKey")
                );
            string cipher = client.Decrypt(kn, ByteString.CopyFromUtf8(plaintext)).ToString();

            return cipher;
        }*/

        public static string Decrypt(string plaintext)
        {
            KeyManagementServiceClient client = KeyManagementServiceClient.Create();
            CryptoKeyName kn = CryptoKeyName.FromUnparsed(
                new Google.Api.Gax.UnparsedResourceName("projects/owenprogforthecloud2020/locations/global/keyRings/owenpfc1/cryptoKeys/pfcKey")
                );

            //byte[] cipherBytes = ByteString.FromBase64(plaintext).ToByteArray();

            string result = client.Decrypt(kn, ByteString.FromBase64(plaintext)).Plaintext.ToStringUtf8();

            //string r = Encoding.Default.GetString(result.ToByteArray());

            return result;
        }
    }
    }
