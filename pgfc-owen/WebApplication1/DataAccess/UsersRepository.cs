﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace WebApplication1.DataAccess
{
    public class UsersRepository: ConnectionClass
    {
        public UsersRepository() : base()
        { }

        public void AddUser(string email, string name, string surname)
        {
            string sql = "INSERT into users (email, name, surname, lastloggedin) values (@email, @name, @surname, @lastloggedin)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@surname", surname);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now); ;


            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }

            cmd.ExecuteNonQuery();

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }
        }

        public bool DoesEmailExist(string email)
        {
            string sql = "Select Count(*) from users where email = @email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }

            bool result = Convert.ToBoolean(cmd.ExecuteScalar());

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }

            return result;
        }

        public void UpdateLastLoggedIn(string email)
        {

            string sql = "update users set lastloggedin = @lastloggedin where email = @email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now); ;

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }

            cmd.ExecuteNonQuery();

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }

        }

    }
}