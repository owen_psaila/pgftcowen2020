﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.DataAccess;
using WebApplication1.Models;
using Google.Cloud.Storage.V1;
using Google.Apis.Storage.v1.Data;
using Google.Cloud.Datastore.V1;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class FilesController : Controller
    {
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(File p, HttpPostedFileBase file)
        {
            try
            {
                var storage = StorageClient.Create();
                string link = "";
                using (var f = file.InputStream)
                {
                    var filename = Guid.NewGuid() + System.IO.Path.GetExtension(file.FileName);
                    var storageObject = storage.UploadObject("pgfc-bucket1", filename, null, f);
                    // link = storageObject.MediaLink;
                    link = "https://storage.cloud.google.com/pgfc-bucket1/" + filename;


                    if (null == storageObject.Acl)
                    {
                        storageObject.Acl = new List<ObjectAccessControl>();
                    }
                    storageObject.Acl.Add(new ObjectAccessControl()
                    {
                        Bucket = "pgfc-bucket1",
                        //Entity = $"user-" + User.Identity.Name,
                        Entity = $"user-" + "owenann31@gmail.com",
                        Role = "OWNER",
                    });

                    storageObject.Acl.Add(new ObjectAccessControl()
                    {
                        Bucket = "pgfc-bucket1",
                        Entity = $"user-" + p.Sharewith,
                        Role = "READER",
                    });

                    var updatedObject = storage.UpdateObject(storageObject, new UpdateObjectOptions()
                    {
                        // Avoid race conditions
                        IfMetagenerationMatch = storageObject.Metageneration,
                    });

                    string cipher = KeyRepository.Encrypt(link);
                    p.OwnerFk = User.Identity.Name; //User.Identity.Name
                    p.Link = cipher;
                    //p.Link = link;
                    FilesRepository pr = new FilesRepository();
                    pr.AddFile(p);

                    CacheRepository cr = new CacheRepository();
                    cr.UpdateCache(pr.GetFiles());
                    
                    PubSubRepository psr = new PubSubRepository();
                    psr.AddToEmailQueue(p); //adding it to queue to be sent as an email later on.

                }
                ViewBag.Message = "Product created successfully";
            }
            catch (Exception e)
            {
                ViewBag.Message = "Product created failed" + e.Message;
                new LogsRepository().LogError(e);
            }

            return RedirectToAction("Index");
        }


        // GET: Product
        public ActionResult Index()
        {
            CacheRepository cr = new CacheRepository();
            var products = cr.GetProductsFromCache(User.Identity.Name);
            //FilesRepository pr = new FilesRepository();
            //var products = pr.GetFiles(User.Identity.Name); //gets products from db
            return View("Index", products);
        }

        public ActionResult Delete(int id)
        {
            new FilesRepository().DeleteFile(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteAll(int[] ids)
        {
            FilesRepository pr = new FilesRepository();
            pr.MyConnection.Open();
            pr.MyTransaction = pr.MyConnection.BeginTransaction();
            try
            {
                foreach (int id in ids)
                {
                    pr.DeleteFile(id);
                }

                pr.MyTransaction.Commit();
                CacheRepository cr = new CacheRepository();
                cr.UpdateCache(pr.GetFiles());

            }
            catch (Exception e)
            {
                pr.MyTransaction.Rollback();//rollback: reverse any changes done in the db back to their original state
                new LogsRepository().LogError(e);
            }

            pr.MyConnection.Close();

            return RedirectToAction("Index");
        }

        public ActionResult DownloadFile(int id) {
            try
            {
                FilesRepository pr = new FilesRepository();
                File product = pr.GetFile(id, User.Identity.Name, User.Identity.Name);
                if (product.Link != null)
                {
                    string realvalue = KeyRepository.Decrypt(product.Link);
                    new LogsRepository().WriteLogEntry(User.Identity.Name + "," + product.Link + "," + DateTime.UtcNow);
                    Response.Redirect(realvalue);
                }
                 else
                 {
                     ViewBag.Message = "You do not have Access";
                     return RedirectToAction("Index", "Files");
                 }
               
            }
            catch(Exception e)
            {
                new LogsRepository().LogError(e);
            }
            return RedirectToAction("Index", "Files");

        }
    }
}